import React, {Component} from 'react';
import logo from './logo.png';
import '../public/style/App.css';
import axios from 'axios';

import BookList from './containers/BookList';

export default class App extends Component {
  /*constructor(props) {
    super(props);

    this.state = {
      books: [],
      selectedBook: null
    };

    this.searchBooks = this.searchBooks.bind(this);
    this.searchBooks();
  }

  searchBooks(query) {
    let getBooks;
    if (query) {
      getBooks = axios.get('/api/search/books', {
        params: {
          q: query
        }
      });
    } else {
      getBooks = axios.get('/api/books');
    }
    getBooks
      .then(res => this.setState({books: res.data.list}))
      .catch(error => console.error("Get books error", error));
  }*/

  render() {
    return (
      <div>
        <div className="App App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <h2>Welcome to ALM Book Store</h2>
        </div>
        <div className="container">
          <div className="row">
            {/*<SearchBar {onSearchQueryChange={this.searchBooks}}/>*/}
          </div>
          <div className="row">
            <div className="col-md-4">
              <BookList
                /*onBookSelect={selectedBook => this.setState({selectedBook})}
                books={this.state.books}*/
              />
            </div>
            <div className="col-md-8">
              {/*<BookDetails
                selectedBook={this.state.selectedBook}
              />*/}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
