import axios from 'axios';

export const SEARCH_BOOKS = "SEARCH_BOOKS";
export const BOOK_SELECTED = "BOOK_SELECTED";
export const SCROLL_BOOKS = "SCROLL_BOOKS";
export const CLEAR_SCROLL_BOOKS = "CLEAR_SCROLL_BOOKS";
export const LOAD_BOOKS = "LOAD_BOOKS";
export const FILTER_BOOKS = "FILTER_BOOKS";
export const SET_SORT = "SET_SORT";
export const CLEAR_ACTIVE_BOOK = "CLEAR_ACTIVE_BOOK";
export const ADD_BOOK = "ADD_BOOK";

export const LOAD_GENRES = "LOAD_GENRES";
export const SET_FILTER = "SET_FILTER";

const SEARCH_BOOKS_URL = "/api/search/books";
const BOOKS_URL = "/api/books";
const FILTER_BOOKS_URL = "/api/books/genre";
const GENRES_URL = "/api/genres";
const ADD_BOOK_URL = "/api/book";

export function selectBook(book) {
  return {
    type: BOOK_SELECTED,
    payload: book
  };
}

export function searchBooks(query) {
  let request;
  if (query) {
    request = axios.get(SEARCH_BOOKS_URL, {
      params: {
        q: query
      }
    });
  } else {
    request = axios.get(BOOKS_URL);
  }

  return {
    type: SEARCH_BOOKS,
    payload: request
  }
}

export function loadMoreBooks(page = 0, sort) {
  const request = axios.get(BOOKS_URL, {
    params: {page, sort}
  });

  return {
    type: SCROLL_BOOKS,
    payload: request
  }
}

export function clearScrollBooks() {
  return {
    type: CLEAR_SCROLL_BOOKS
  }
}

export function loadPageBooks(page = 0, sort) {
  const request = axios.get(BOOKS_URL, {
    params: {page, sort}
  });

  return {
    type: LOAD_BOOKS,
    payload: request
  }
}

export function setSort(sort) {
  return {
    type: SET_SORT,
    payload: sort
  }
}

export function clearActiveBook() {
  return {
    type: CLEAR_ACTIVE_BOOK
  }
}

export function loadGenreList(page = 0) {
  const request = axios.get(GENRES_URL, {
    params: {page, sort: "[name=asc]"}
  });

  return {
    type: LOAD_GENRES,
    payload: request
  }
}

export function filterBooks(page = 0, sort, genreId) {
  const path = `${FILTER_BOOKS_URL}/${genreId}`;
  const request = axios.get(path, {
    params: {page, sort}
  });

  return {
    type: FILTER_BOOKS,
    payload: request
  }
}

export function setFilter(genreId) {
  return {
    type: SET_FILTER,
    payload: genreId
  }
}

export function addBook(book, callback) {
  const request = axios.post(ADD_BOOK_URL, book).then(() => callback());
  return {
    type: ADD_BOOK,
    payload: request
  }
}