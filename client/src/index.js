import React from 'react';
import ReactDOM from 'react-dom';

import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from 'redux';
import ReduxPromise from 'redux-promise';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import reducers from './reducers';
import BooksSearch from './components/BooksSearch';
import BooksScroll from './components/BooksScroll';
import BooksPagination from './components/BooksPagination';
import BooksFilter from './components/BooksFilter';
import BookAdd from './components/admin/BookAdd';
import NotFound from './components/NotFound';
import Root from './components/Root';

import registerServiceWorker from './registerServiceWorker';

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);

registerServiceWorker();
ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <Root>
        <Switch>
          <Route path="/" exact component={BooksSearch}/>
          <Route path="/books-pagination" component={BooksPagination}/>
          <Route path="/books-scroll" component={BooksScroll}/>
          <Route path="/books-filter" component={BooksFilter}/>
          <Route path="/admin/book-add" component={BookAdd}/>
          <Route component={NotFound}/>
        </Switch>
      </Root>
    </BrowserRouter>
  </Provider>,
  document.querySelector("#root")
);
