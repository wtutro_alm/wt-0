import React from 'react';
import _ from 'lodash';

import GenreSelectBox from '../containers/GenreSelectBox';

export default class Utils {

  static renderInputField(field) {
    const {meta: {touched, error}} = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;
    const type = field.type ? field.type : "text";

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type={type} {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }

  static renderSelectGenreField(field) {
    const {meta: {touched, error}} = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <GenreSelectBox select={field.input}/>
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }

  static validateBookNewForm(values) {
    const errors = {};

    // Validate the inputs from 'values'
    if (!values.title) {
      errors.title = "Enter a title";
    }
    if (!values.ISBN) {
      errors.ISBN = "Enter ISBN";
    }
    if (!values.price && _.isNumber(values.price)) {
      errors.content = "Enter some price";
    }
    if (!values.publicationYear && _.isNumber(values.publicationYear)) {
      errors.content = "Enter some publication year";
    }
    if (!values.author.firstName) {
      errors.content = "Enter author first name";
    }
    if (!values.author.lastName) {
      errors.content = "Enter author last name";
    }

    return errors;
  }
}