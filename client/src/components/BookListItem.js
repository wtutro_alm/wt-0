import React from 'react';

const BookListItem = ({book, selectBook, activeBookId, showPrice = false}) => {

  return (
    <a className={`list-group-item list-group-item-action ${activeBookId === book.id ? 'active' : ''}`}
       key={book.id}
       onClick={() => selectBook(book)}>
      <h5>
        {book.title}
        <span className={`float-right ${showPrice ? '' : 'invisible'}`}>
          Price: ${book.price.toFixed(2)}
        </span>
      </h5>
      <p>
        {book.author.firstName} {book.author.lastName}
      </p>
    </a>
  );
};

export default BookListItem;