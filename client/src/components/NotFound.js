import React from 'react';

const NotFound = () => {
  return (
    <div>
      <h1>Page not found</h1>
      <p>We are sorry but the requested page has not been found.</p>
    </div>
  );
};

export default NotFound;