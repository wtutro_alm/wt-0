import React from 'react';
import {NavLink} from 'react-router-dom';

const Nav = () => {
  return (
    <div className="container">
      <nav className="nav">
        <NavLink
          exact
          strict
          to="/"
          activeClassName="active"
          className="nav-link"
        >Books search</NavLink>

        <NavLink
          exact
          strict
          to="/books-scroll"
          activeClassName="active"
          className="nav-link"
        >Books with scrollingg</NavLink>

        <NavLink
          exact
          strict
          to="/books-pagination"
          activeClassName="active"
          className="nav-link"
        >Books with pagination</NavLink>

        <NavLink
          exact
          strict
          to="/books-filter"
          activeClassName="active"
          className="nav-link"
        >Books with filters</NavLink>
      </nav>
    </div>
  );
};

export default Nav;