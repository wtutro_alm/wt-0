import React, {Component} from 'react';
import BookList from '../containers/BookList';
import BookDetails from '../containers/BookDetails';
import SearchBar from '../containers/SearchBar';

export default class BooksSearch extends Component {

  render() {
    return (
      <div className="mt-3">
        <div className="row mt-3">
          <SearchBar/>
        </div>
        <div className="row mt-3">
          <div className="col-md-4">
            <BookList/>
          </div>
          <div className="col-md-8">
            <BookDetails/>
          </div>
        </div>
      </div>
    );
  }
}