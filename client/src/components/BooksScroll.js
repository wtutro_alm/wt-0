import React, {Component} from 'react';

import BookListWithScroll from '../containers/BookListWithScroll';
import BookDetailsModal from '../containers/BookDetailsModal';

export default class BooksScroll extends Component {
  render() {
    return (
      <div className="row mt-3">
        <BookListWithScroll/>
        <BookDetailsModal/>
      </div>
    );
  }
}