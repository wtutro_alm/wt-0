import React, {Component} from 'react';

import BookDetailsModal from '../containers/BookDetailsModal';
import BookListWithFilter from '../containers/BookListWithFilter';

export default class BooksFilter extends Component {

  render() {
    return (
      <div>
        <div className="row mt-3">
          <BookListWithFilter/>
          <BookDetailsModal/>
        </div>
      </div>
    );
  }
}