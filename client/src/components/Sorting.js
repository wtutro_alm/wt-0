import React from 'react';

const Sorting = ({onSortSelected, sort}) => {

  return (
    <div className="btn-group">
      <button type="button" className="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Sorting: {sort}
      </button>
      <div className="dropdown-menu">
        <a className="dropdown-item" onClick={() => onSortSelected('[title=asc]')}>Title (ASC)</a>
        <a className="dropdown-item" onClick={() => onSortSelected('[title=desc]')}>Title (DESC)</a>
        <div className="dropdown-divider"/>
        <a className="dropdown-item" onClick={() => onSortSelected('[price=asc]')}>Price (ASC)</a>
        <a className="dropdown-item" onClick={() => onSortSelected('[price=desc]')}>Price (DESC)</a>
      </div>
    </div>
  );
};

export default Sorting;