import React, {Component} from 'react';

import BookListWithPagination from '../containers/BookListWithPagination';
import BookDetailsModal from '../containers/BookDetailsModal';

export default class BooksPagination extends Component {
  render() {
    return (
      <div className="row mt-3">
        <BookListWithPagination />
        <BookDetailsModal/>
      </div>
    );
  }
}