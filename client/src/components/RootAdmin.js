import React from 'react';

import logo from '../logo.png';
import Nav from './Nav';

const RootAdmin = props => {
  return (
    <div>
      <div>
        <div className="App App-header">
          <img src={logo} className="App-logo-admin" alt="logo"/>
          <h2>Welcome to Admin Panel</h2>
        </div>
        <Nav />
        <div className="container">
          {props.children}
        </div>
      </div>
    </div>
  );
};

export default RootAdmin;