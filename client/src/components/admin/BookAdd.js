import React, {Component} from 'react';

import BookNew from '../../containers/BookNew';

export default class BookAdd extends Component {
  render() {
    return (
      <div className="row mt-3">
        <BookNew/>
      </div>
    );
  }
}