import React from 'react';

import logo from '../logo.png';
import Nav from './Nav';

const Root = props => {
  return (
    <div>
      <div className="App App-header">
        <img src={logo} className="App-logo" alt="logo"/>
        <h2>Welcome to ALM Book Store</h2>
      </div>
      <Nav/>
      <div className="container">
        {props.children}
      </div>
    </div>
  );
};

export default Root;