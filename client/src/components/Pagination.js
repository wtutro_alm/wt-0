import React from 'react';

const Pagination = ({currentPage, total, delta, onPageClick}) => {

  const pagesCount = Math.ceil(total / delta);

  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination">
        {renderPaginationItem(currentPage, pagesCount, onPageClick)}
      </ul>
    </nav>
  );
};

function renderPaginationItem(currentPage, pagesCount, onPageClick) {
  if (!pagesCount) {
    return
  }
  let pagesArray = Array.from(new Array(pagesCount).keys());
  return pagesArray.map((i) => {
    return (
      <li key={i} className={`page-item ${currentPage === i ? 'active' : ''}`}>
        <button className="page-link" onClick={() => onPageClick(i)}>{i + 1}</button>
      </li>
    );
  });
}

export default Pagination;