import {CLEAR_SCROLL_BOOKS, LOAD_BOOKS, SCROLL_BOOKS, SEARCH_BOOKS, SET_SORT, FILTER_BOOKS, SET_FILTER} from '../actions/index';

export default (state = {page: 0, list: []}, action) => {
  switch (action.type) {
    case SEARCH_BOOKS:
      return {
        page: action.payload.data.page,
        total: action.payload.data.total,
        delta: action.payload.data.delta,
        list: action.payload.data.list
      };
    case SCROLL_BOOKS:
      return {
        list: [...state.list, ...action.payload.data.list],
        page: action.payload.data.page,
        total: action.payload.data.total,
        delta: action.payload.data.delta
      };
    case LOAD_BOOKS:
      return {
        ...state,
        page: action.payload.data.page,
        total: action.payload.data.total,
        delta: action.payload.data.delta,
        list: action.payload.data.list
      };
    case FILTER_BOOKS:
      return {
        ...state,
        page: action.payload.data.page,
        total: action.payload.data.total,
        delta: action.payload.data.delta,
        list: action.payload.data.list
      };
    case CLEAR_SCROLL_BOOKS:
      return {
        page: 0,
        list: []
      };
    case SET_SORT:
      return {
        ...state,
        sort: action.payload
      };
    case SET_FILTER:
      return {
        ...state,
        genreId: action.payload
      };
    default:
      return state;
  }
}