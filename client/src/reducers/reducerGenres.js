import {LOAD_GENRES} from "../actions/index";

export default (state = {page: 0, list: []}, action) => {
  switch (action.type) {
    case LOAD_GENRES:
      return {
        ...state,
        page: action.payload.data.page,
        total: action.payload.data.total,
        delta: action.payload.data.delta,
        list: action.payload.data.list
      };
    default:
      return state;
  }
}