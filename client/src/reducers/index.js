import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

import BooksReducer from './reducerBooks';
import ActiveBook from './reducerActiveBook';
import GenresReducer from './reducerGenres';

const rootReducer = combineReducers({
  books: BooksReducer,
  activeBook: ActiveBook,
  genres: GenresReducer,
  form: formReducer
});

export default rootReducer;