import {BOOK_SELECTED, CLEAR_ACTIVE_BOOK} from '../actions';

export default (state = null, action) => {
  switch (action.type) {
    case BOOK_SELECTED:
      return action.payload;
    case CLEAR_ACTIVE_BOOK:
      return null;
    default:
      return state;
  }
}