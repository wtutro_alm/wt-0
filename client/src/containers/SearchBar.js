import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {searchBooks} from "../actions/index";

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {query: ""};

    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onInputChange(event) {
    let query = event.target.value;
    this.setState({query});
  }

  onFormSubmit(event) {
    event.preventDefault();
    this.props.searchBooks(this.state.query);
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit} className="input-group col">
        <input
          placeholder="Search books"
          className="form-control"
          value={this.state.query}
          onChange={this.onInputChange}
        />
        <span className="input-group-btn">
          <button type="submit" className="btn btn-secondary">Submit</button>
        </span>
      </form>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({searchBooks}, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);