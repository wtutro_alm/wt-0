import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Waypoint from 'react-waypoint';

import BookListItem from '../components/BookListItem';
import {loadMoreBooks, clearScrollBooks, selectBook} from "../actions/index";

class BookListWithScroll extends Component {
  constructor(props) {
    super(props);

    this.renderBooks = this.renderBooks.bind(this);
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    this.props.clearScrollBooks();
    this.props.loadMoreBooks(0);
  }

  renderBooks() {
    if (!this.props.books.list) {
      return [];
    }
    return this.props.books.list.map((book, i) => {
      return (
        <BookListItem
          key={i}
          book={book}
          showPrice={true}
          selectBook={this.props.selectBook}
        />
      );
    });
  }

  loadMore() {
    if (this.hasMore() && this.props.books.total > 0) {
      return this.props.loadMoreBooks(this.props.books.page + 1, this.props.books.sort);
    }
  }

  hasMore() {
    if (!this.props.books.total) {
      return true;
    } else {
      return this.props.books.page * this.props.books.delta < this.props.books.total;
    }
  }

  render() {
    return (
      <div className="col-md-12">
        {this.renderBooks()}
        <Waypoint
          onEnter={this.loadMore}>
        </Waypoint>
      </div>
    );
  }
}

function mapStateToProps({books}) {
  return {books};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({loadMoreBooks, clearScrollBooks, selectBook}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookListWithScroll);