import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import BookListItem from '../components/BookListItem';
import {loadPageBooks, setSort, selectBook} from '../actions/index';
import Pagination from '../components/Pagination';
import Sorting from '../components/Sorting';

class BookListWithPagination extends Component {

  constructor() {
    super();

    this.onSortSelected = this.onSortSelected.bind(this);
    this.onPageClick = this.onPageClick.bind(this);
  }

  componentDidMount() {
    this.props.loadPageBooks();
  }

  onPageClick(page) {
    this.props.loadPageBooks(page, this.props.books.sort);
  }

  onSortSelected(type) {
    this.props.setSort(type);
    this.props.loadPageBooks(0, type);
  }

  renderList() {
    return this.props.books.list.map((book, i) => {
      return (
        <BookListItem
          key={i}
          book={book}
          showPrice={true}
          selectBook={this.props.selectBook}
        />
      );
    });
  }

  render() {
    if (!this.props.books.list) {
      return <div>Loading...</div>
    }
    return (
      <div className="col-md-12">
        <div className="row mb-3">
          <div className="col">
            <Sorting
              onSortSelected={this.onSortSelected}
              sort={this.props.books.sort}
            />
          </div>
        </div>
        <div className="row mb-3">
          <div className="col">
            <Pagination
              currentPage={this.props.books.page}
              total={this.props.books.total}
              delta={this.props.books.delta}
              onPageClick={this.onPageClick}
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            {this.renderList()}
          </div>
        </div>
        <div className="row mt-3">
          <div className="col">
            <Pagination
              currentPage={this.props.books.page}
              total={this.props.books.total}
              delta={this.props.books.delta}
              onPageClick={this.onPageClick}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({books}) {
  return {books};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({loadPageBooks, setSort, selectBook}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookListWithPagination);