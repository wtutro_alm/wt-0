import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import Modal from 'react-modal';

import {clearActiveBook} from "../actions/index";

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

class BookDetailsModal extends Component {

  constructor() {
    super();

    this.state = {
      modalIsOpen: false
    }

    this.closeModal = this.closeModal.bind(this);
  }

  closeModal() {
    this.props.clearActiveBook();
  }

  render() {
    if (!this.props.activeBook) {
      return null;
    }
    return (
      <Modal
        isOpen={this.props.activeBook !== null}
        contentLabel="Modal"
        style={customStyles}
      >
        <h1>{this.props.activeBook.title}</h1>
        <p>{this.props.activeBook.author.firstName} {this.props.activeBook.author.lastName}</p>
        <div className="btn-group">
          <button className="btn btn-primary" onClick={this.closeModal}>Close</button>
        </div>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    activeBook: state.activeBook
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({clearActiveBook}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookDetailsModal);