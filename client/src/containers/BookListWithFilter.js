import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {filterBooks, selectBook} from '../actions/index';
import GenresFilter from './GenresFilter';
import Pagination from '../components/Pagination';
import BookListItem from '../components/BookListItem';

class BookListWithFilter extends Component {

  constructor() {
    super();

    this.onPageClick = this.onPageClick.bind(this);
  }

  componentDidMount() {
    this.props.filterBooks(0, null, null);
  }

  onPageClick(page) {
    this.props.filterBooks(page, null, this.props.books.genreId);
  }

  renderList() {
    return this.props.books.list.map((book, i) => {
      return (
        <BookListItem
          key={i}
          book={book}
          showPrice={true}
          selectBook={this.props.selectBook}
        />
      );
    });
  }

  render() {
    if (!this.props.books.list) {
      return <div>Loading...</div>
    }
    return (
      <div className="col-md-12">
        <div className="row mb-3">
          <div className="col">
            <GenresFilter/>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col">
            <Pagination
              currentPage={this.props.books.page}
              total={this.props.books.total}
              delta={this.props.books.delta}
              onPageClick={this.onPageClick}
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            {this.renderList()}
          </div>
        </div>
        <div className="row mt-3">
          <div className="col">
            <Pagination
              currentPage={this.props.books.page}
              total={this.props.books.total}
              delta={this.props.books.delta}
              onPageClick={this.onPageClick}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({books}) {
  return {books};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({filterBooks, selectBook}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookListWithFilter);