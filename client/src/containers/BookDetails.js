import React, {Component} from 'react';
import {connect} from 'react-redux';

class BookDetails extends Component {
  render() {
    if (!this.props.book) {
      return <div>Please select book on the left</div>
    }
    return (
      <div className="jumbotron">
        <div className="container">
          <h2 className="display-5">{this.props.book.title}</h2>
          <p className="lead">
            Author: {this.props.book.author.firstName} {this.props.book.author.lastName}<br/>
            Publication year: {this.props.book.publicationYear}
          </p>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    book: state.activeBook
  };
}

export default connect(mapStateToProps)(BookDetails);