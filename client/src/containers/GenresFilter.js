import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {loadGenreList, filterBooks, setFilter} from "../actions/index";

class GenresFilter extends Component {

  componentDidMount() {
    this.props.loadGenreList();
  }

  onFilterSelect(genreId) {
    this.props.setFilter(genreId);
    this.props.filterBooks(0, null, genreId);
  }

  render() {
    return (
      <div className="btn-group" role="group" aria-label="Genre filter">
        {this.props.genres.list.map((item, i) => {
            return <button key={i} type="button" className="btn btn-secondary" onClick={() => this.onFilterSelect(item.id)}>{item.name}</button>
          }
        )}
      </div>
    );
  }
}

function mapStateToProps({genres}) {
  return {genres};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({loadGenreList, filterBooks, setFilter}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GenresFilter);