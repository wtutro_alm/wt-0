import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {addBook} from "../actions/index";
import Utils from '../utils/Utils'

class BookNew extends Component {

  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(values) {
    console.log(values);
    this.props.addBook(values, () => {
      console.log("Book has been added: ")
    })
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <form onSubmit={handleSubmit(this.onSubmit)}>
        <Field
          label="Title"
          name="title"
          component={Utils.renderInputField}
        />
        <Field
          label="ISBN"
          name="ISBN"
          component={Utils.renderInputField}
        />
        <Field
          label="Price"
          name="price"
          type="number"
          component={Utils.renderInputField}
        />
        <Field
          label="Publication year"
          name="publicationYear"
          type="number"
          component={Utils.renderInputField}
        />
        <hr/>
        <Field
          label="Author first name"
          name="author.firstName"
          component={Utils.renderInputField}
        />
        <Field
          label="Author last name"
          name="author.lastName"
          component={Utils.renderInputField}
        />
        <Field
          label="Genre"
          name="genres.id"
          component={Utils.renderSelectGenreField}
        />
        <button type="submit">Submit</button>
      </form>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({addBook}, dispatch)
}

export default reduxForm({
  validate: Utils.validateBookNewForm,
  form: 'BookNewForm',
  initialValues: {"genres": {"id": 4}}
})(connect(null, mapDispatchToProps)(BookNew));