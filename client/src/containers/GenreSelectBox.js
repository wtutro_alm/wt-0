import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {loadGenreList} from "../actions/index";

class GenreSelectBox extends Component {
  componentDidMount() {
    this.props.loadGenreList();
  }

  render() {
    return (
      <select className="form-control" {...this.props.select}>
        {this.props.genres.list.map((item, i) => {
            return <option key={i} value={item.id}>{item.name}</option>;
          }
        )}
      </select>
    );
  }
}

function mapStateToProps({genres}) {
  return {genres};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({loadGenreList}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GenreSelectBox);