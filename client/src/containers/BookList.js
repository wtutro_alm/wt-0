import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import BookListItem from '../components/BookListItem';
import {selectBook, searchBooks} from '../actions/index';

class BookList extends Component {

  componentDidMount() {
    this.props.searchBooks();
  }

  renderList() {
    const selectedId = this.props.activeBook ? this.props.activeBook.id : null;
    return this.props.books.list.map(book => {
      return (
        <BookListItem
          selectBook={this.props.selectBook}
          activeBookId={selectedId}
          key={book.id}
          book={book}
        />
      );
    });
  }

  render() {
    if (!this.props.books) {
      return <div>Loading...</div>
    }
    return (
      <div className="list-group">
        {this.renderList()}
      </div>
    );
  }
}

function mapStateToProps({books, activeBook}) {
  return {books, activeBook};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({selectBook, searchBooks}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookList);