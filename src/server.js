'use strict';

const http = require('http');
const app = require('./app');

let server;

app.set('port', process.env.PORT || 5000);


// ******************************
// ***** Initialize Server ******
// ******************************

function startServer() {
  server = http.createServer(app).listen(app.get('port'), function () {
    console.log('Express started in ' + app.get('env') +
      ' mode on http://localhost:' + app.get('port') +
      '; press Ctrl-C to terminate.');
  });
}

if (require.main === module) {
  // application run directly; start app server
  startServer();
} else {
  // application imported as a module via "require": export function to create server
  module.exports = startServer;
}