'use strict';

module.exports = {
  "analysis": {
    "analyzer": {
      "alm_analyzer": {
        "tokenizer": "whitespace",
        "filter": ["lowercase", "word_delimiter"]
      }
    }
  }
};