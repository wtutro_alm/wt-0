'use strict';

const {bookMapping, genreMapping} = require('./mappings');
const settings = require('./settings');

module.exports = {
  "settings": settings,
  "mappings": {
    "genre": genreMapping,
    "book": bookMapping
  }
};