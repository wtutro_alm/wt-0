'use strict';

module.exports.bookMapping = {
  "_all": {
    "enabled": true
  },
  "dynamic": "strict",
  "properties": {
    "title": {
      "type": "text",
      "analyzer": "alm_analyzer",
      "fields": {
        "keyword": {
          "type": "keyword"
        }
      }
    },
    "ISBN": {
      "type": "keyword"
    },
    "price": {
      "type": "scaled_float",
      "scaling_factor": 100
    },
    "publicationYear": {
      "type": "date",
      "format": "yyyy"
    },
    "author": {
      "type": "nested",
      "include_in_all": false,
      "properties": {
        "firstName": {
          "type": "keyword"
        },
        "lastName": {
          "type": "keyword"
        }
      }
    },
    "genres": {
      "properties": {
        "id": {
          "type": "keyword"
        }
      }
    },
    "created": {
      "type": "date"
    }
  }
};

module.exports.genreMapping = {
  "_all": {
    "enabled": true
  },
  "dynamic": "strict",
  "properties": {
    "name": {
      "type": "text",
      "analyzer": "alm_analyzer",
      "fields": {
        "keyword": {
          "type": "keyword"
        }
      }
    },
    "description": {
      "type": "text",
      "analyzer": "alm_analyzer",
    },
    "created": {
      "type": "date"
    }
  }
};