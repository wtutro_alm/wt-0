'use strict';

// prod.js - production settings here!!
module.exports = {
  cookieSecretKey: process.env.COOKIE_SECRET_KEY,
  indexName: "product_index",
  elasticSearchHost: "localhost:9200",
  elasticSearchLogLevel: 'info',
  forceCreateIndex: false,
  initSampleData: false
};
