'use strict';

const elasticSearchService = require('../services/ElasticSearchService');

module.exports = {
  books: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const result = await elasticSearchService.listBooks({
        page: req.query.page ? parseInt(req.query.page, 10) : req.query.page,
        sort: req.query.sort
      }).catch(error => {
        console.warn(error);
        return {list: []};
      });
      res.json(result);
    }
  },

  bookById: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const result = await elasticSearchService.getBookById(req.params.id)
        .catch(error => {
          console.warn(error);
          return {};
        });
      res.json(result);
    }
  },

  booksByGenreId: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const result = await elasticSearchService.listBooks({
        page: req.query.page ? parseInt(req.query.page, 10) : req.query.page,
        size: 10,
        genreId: parseInt(req.params.id, 10)
      }).catch(error => {
        console.warn(error);
        return {list: []};
      });
      res.json(result);
    }
  },

  searchBooks: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const result = await elasticSearchService.listBooks({
        page: req.query.page ? parseInt(req.query.page, 10) : req.query.page,
        size: 10,
        q: req.query.q,
        sort: req.query.sort
      }).catch(error => {
        console.warn(error);
        return {list: []};
      });
      res.json(result);
    }
  },

  addBook: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      console.log(validationResult.mapped());
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const result = await elasticSearchService.saveBook(req.body)
        .catch(error => {
          console.warn(error);
          return {};
        });
      res.json(result);
    }
  },

  updateBook: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const book = req.body;
      book.id = parseInt(req.params.id, 10);
      const result = await elasticSearchService.updateBook(req.body)
        .catch(error => {
          console.warn(error);
          return {};
        });
      res.json(result);
    }
  }
};