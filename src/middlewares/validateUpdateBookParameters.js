'use strict';

module.exports = (req, res, next) => {
  req.check('title', 'Please provide valid title').optional().isAscii();
  req.check('ISBN', 'Please provide valid ISBN').optional().isAscii();
  req.check('price', 'Please provide valid price').optional().isFloat();
  req.check('publicationYear', 'Please provide valid publicationYear').optional().isNumeric();
  req.check('author.firstName', 'Please provide author first name').optional().isAscii();
  req.check('author.lastName', 'Please provide author last name').optional().isAscii();
  req.check('genres', 'Please provide genres').optional().isAscii();
  next();
};