'use strict';

module.exports = (req, res, next) => {
  req.check('id').exists().withMessage('id parameter is required ').isAscii().withMessage('Invalid id value');
  next();
};