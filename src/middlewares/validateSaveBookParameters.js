'use strict';

module.exports = (req, res, next) => {
  req.check('title', 'Please provide valid title').isAscii();
  req.check('ISBN', 'Please provide valid ISBN').isAscii();
  req.check('price', 'Please provide valid price').isFloat();
  req.check('publicationYear', 'Please provide valid publicationYear').isNumeric();
  req.check('author.firstName', 'Please provide author first name').isAlphanumeric();
  req.check('author.lastName', 'Please provide author last name').isAlphanumeric();
  req.check('genres', 'Please provide genres').isAscii();
  next();
};