'use strict';

module.exports = (req, res, next) => {
  req.check('name', 'Please provide valid title').isAscii();
  req.check('description', 'Please provide valid description').isAscii();
  next();
};