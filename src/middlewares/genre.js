'use strict';

const elasticSearchService = require('../services/ElasticSearchService');

module.exports = {
  genres: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const result = await elasticSearchService.listGenre(
        req.query.page ? parseInt(req.query.page, 10) : req.query.page,
        10,
        req.query.sort).catch(empty => res.json(empty)
      );
      res.json(result);
    }
  },

  genreById: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const result = await elasticSearchService.getGenreById(parseInt(req.params.id, 10)).catch(empty => res.json(empty));
      res.json(result);
    }
  },

  saveOrUpdate: async (req, res) => {
    const validationResult = await req.getValidationResult();
    if (!validationResult.isEmpty()) {
      res.statusCode = 500;
      res.json(validationResult.mapped());
    } else {
      const result = await elasticSearchService.saveOrUpdateGenre(req.body).catch(empty => res.json(empty));
      res.json(result);
    }
  }
};