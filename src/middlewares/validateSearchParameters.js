'use strict';

module.exports = (req, res, next) => {
  req.check('q').exists().withMessage('Query parameter is required ').isAlphanumeric().withMessage('Invalid query value');
  req.check('page', 'Parameter page must be a number').optional().isNumeric();
  req.check('sort', 'Parameter sort must provide correct field_name=asc|desc pair').optional().matches('\\[(\\w+=(asc|desc){1},?)+\\]');
  next();
};