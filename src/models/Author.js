'use strict';

class Author {
  constructor(properties) {
    if (properties) {
      this.firstName = properties.firstName;
      this.lastName = properties.lastName;
    }
  }
}

module.exports = Author;