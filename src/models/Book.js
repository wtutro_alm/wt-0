'use strict';

const _ = require('lodash');

const Author = require('./Author');

class Book {
  constructor(properties) {
    if (properties) {
      this.id = properties.id;
      this.title = properties.title;
      this.ISBN = properties.ISBN;
      this.price = _.toNumber(properties.price);
      this.publicationYear = _.toNumber(properties.publicationYear);
      this.author = new Author(properties.author);
      this.genres = properties.genres;
      this.created = properties.created;
    } else {
      this.created = new Date();
    }
  }

  static createFromElasticSearchResult(result) {
    return result.hits.hits.map(function (item) {
      return new Book({
        id: item._id,
        title: item._source.title,
        ISBN: item._source.ISBN,
        price: _.toNumber(item._source.price),
        publicationYear: _.toNumber(item._source.publicationYear),
        author: item._source.author,
        genres: item._source.genres,
        created: item._source.created
      });
    });
  }
}

module.exports = Book;