'use strict';

class Genre {
  constructor(properties) {
    if (properties) {
      this.id = properties.id;
      this.name = properties.name;
      this.description = properties.description;
      this.created = properties.created;
    } else {
      this.created = new Date();
    }
  }

  static createFromElasticSearchResult(result) {
    return result.hits.hits.map(function (item) {
      return new Genre({
        id: item._id,
        name: item._source.name,
        description: item._source.description,
        created: item._source.created
      });
    });
  }

  toJSON() {
    return {
      "id": this.id,
      "name": this.name,
      "description": this.description,
      "created": this.created
    }
  }
}

module.exports = Genre;