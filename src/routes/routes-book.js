'use strict';

const validateListParameters = require('../middlewares/validateListParameters');
const validateSaveBookParameters = require('../middlewares/validateSaveBookParameters');
const validateUpdateBookParameters = require('../middlewares/validateUpdateBookParameters');
const requireId = require('../middlewares/requireId');
const bookHandler = require('../middlewares/book');

module.exports = (apiRouter) => {
  apiRouter.get('/books', validateListParameters, bookHandler.books);
  apiRouter.get('/book/:id', requireId, bookHandler.bookById);
  apiRouter.get('/books/genre/:id', requireId, bookHandler.booksByGenreId);
  apiRouter.get('/search/books', validateListParameters, bookHandler.searchBooks);
  apiRouter.put('/book/:id', validateUpdateBookParameters, bookHandler.updateBook);
  apiRouter.post('/book', validateSaveBookParameters, bookHandler.addBook);
};