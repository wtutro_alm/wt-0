'use strict';

const validateListParameters = require('../middlewares/validateListParameters');
const validateSaveOrUpdateGenreParameters = require('../middlewares/validateSaveOrUpdateGenreParameters');
const requireId = require('../middlewares/requireId');
const genreHandler = require('../middlewares/genre');

module.exports = (apiRouter) => {

  apiRouter.get('/genres', validateListParameters, genreHandler.genres);
  apiRouter.get('/genre/:id', requireId, genreHandler.genreById);
  apiRouter.put('/genre/:id', validateSaveOrUpdateGenreParameters, genreHandler.saveOrUpdate);
  apiRouter.post('/genre', validateSaveOrUpdateGenreParameters, genreHandler.saveOrUpdate);
};