'use strict';

const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const {cookieSecretKey} = require('./config/properties');
const expressValidator = require('express-validator');
const elasticSearchService = require('./services/ElasticSearchService');

const app = express();

app.use(require('morgan')(app.get('env') === 'production' ? 'prod' : 'dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressValidator());

app.use(cookieParser(cookieSecretKey));

// init connection with ElasticSearch
elasticSearchService.initConnection();
elasticSearchService.init().then(() => console.info("Initialization completed"));

// ******************************
// ************ API *************
// ******************************
let apiRouter = express.Router();

// API routes
require('./routes/routes-book')(apiRouter);
require('./routes/routes-genre')(apiRouter);

// link API into pipeline
app.use('/api', apiRouter);

// ******************************
// ******* Error handling **********
// ******************************
// 404 catch-all handler (middlewares)
app.use(function (req, res, next) {
  res.status(404);
  res.send();
});

// 500 error handler (middlewares)
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500);
  res.send();
});

module.exports = app;