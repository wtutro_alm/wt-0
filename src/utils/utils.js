'use strict';

const queryBuilder = require('elastic-builder');

exports.getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

exports.prepareMatchQuery = (q) => {
  let query = undefined;
  if (q) {
    let queryBody = queryBuilder.requestBodySearch()
      .query(queryBuilder.fuzzyQuery('_all', q).fuzziness(4));
    query = queryBody.toJSON();
  }
  return query;
};

exports.prepareSortParameters = (sort) => {
  if (sort) {
    let sortParameterObj = resolveSortParameter(sort);
    let sortArray = [];
    for (let [key, value] of entries(sortParameterObj)) {
      let fieldName = key;
      if (key === "title" || key === "name") {
        fieldName += ".keyword";
      }
      sortArray.push(fieldName + ":" + value);
    }
    return sortArray;
  }
  return undefined;
};

function resolveSortParameter(sort) {
  if (sort) {
    return JSON.parse('{"' + sort.substr(1, sort.length - 2).replace(/,/g, '", "').replace(/=/g, '": "') + '"}');
  }
}

function* entries(obj) {
  for (let key of Object.keys(obj)) {
    yield [key, obj[key]];
  }
}

module.exports.entries = entries;

exports.calculateFrom = (page, delta) => {
  return page * delta;
};