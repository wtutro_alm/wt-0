'use strict';

const elasticSearch = require('elasticsearch');
const utils = require('../utils/utils');
const bookTitles = require('../samples/bookTitles');
const {firstName, lastName} = require('../samples/names');
const genres = require('../samples/genres');
const indexBody = require('../config/index/indexBody');
const Book = require('../models/Book');
const Genre = require('../models/Genre');
const uuidv1 = require('uuid/v1');
const _ = require('lodash');

let elasticClient;

class ElasticSearchService {

  constructor({elasticSearchHost, elasticSearchLogLevel, indexName, forceCreateIndex = false, initSampleData = false}) {
    this.elasticSearchHost = elasticSearchHost;
    this.elasticSearchLogLevel = elasticSearchLogLevel;
    this.indexBody = indexBody;
    this.indexName = indexName;
    this.forceCreateIndex = forceCreateIndex;
    this.initSampleData = initSampleData;
  }

  static async ping() {
    return elasticClient.ping({
      requestTimeout: 30000,
    });
  }

  async init() {
    if (!elasticClient) {
      console.log("ElasticSearch connection is not initialized. Please invoke initConnection first");
      return;
    }
    console.log("***** Initializing ElasticSearch *****");
    const isRunning = await ElasticSearchService.ping().catch(error => console.error("ElasticSearch is down. Please start ES server... ", error));

    if (isRunning) {
      console.log("ElasticSearch is up and running...");
      const exists = await elasticSearchService.indexExists();
      if (exists && elasticSearchService.forceCreateIndex) {
        console.log("Index already exists. Deleting index...");
        await elasticSearchService.deleteIndex();

        console.log("Creating index and initializing index");
        await elasticSearchService.initIndex();
      } else if (!exists) {
        console.log("Creating index and initializing index");
        await elasticSearchService.initIndex();
      } else {
        console.log("Index already exists. Nothing to do...");
      }

      if (elasticSearchService.initSampleData) {
        console.log("Populating ES with sample data...");
        await elasticSearchService.populateSampleData();
        console.log("Populating has been completed. System is up and running.");
      } else {
        console.log("No need to populate data");
      }
    }
  }

  initConnection() {
    return elasticClient = new elasticSearch.Client({
      host: this.elasticSearchHost,
      log: this.elasticSearchLogLevel
    });
  }

  async deleteIndex() {
    return elasticClient.indices.delete({
      index: this.indexName
    });
  }

  async initIndex() {
    return elasticClient.indices.create({
      index: this.indexName,
      body: this.indexBody
    });
  }

  async indexExists() {
    return elasticClient.indices.exists({
      index: this.indexName
    });
  }

  // there is also create/update method to this in ES API but genre is so small
  // that there will be no significant effort on this
  async saveOrUpdateGenre(genre) {
    try {
      const result = await elasticClient.index({
        index: this.indexName,
        id: genre.id ? genre.id : uuidv1(),
        type: "genre",
        body: {
          "name": genre.name,
          "description": genre.description,
          "created": genre.created ? new Date() : genre.created
        }
      });

      console.log('Creating filtered alias for genre [id=' + result._id + ']');
      await elasticClient.indices.putAlias({
        index: elasticSearchService.indexName,
        name: "genre_" + result._id,
        body: {
          "actions": [
            {
              "add": {
                "filter": {"term": {"genres.id": result._id}}
              }
            }
          ]
        }
      }).catch(error => console.error("Error creating aliases for genre", error));

      return result
    } catch (error) {
      console.error(error);
    }
  }

  async saveOrUpdateBook(book) {
    if (book.id) {
      return this.updateBook(book);
    } else {
      return this.saveBook(book);
    }
  }

  async saveBook(book) {
    return elasticClient.create({
      index: this.indexName,
      id: uuidv1(),
      type: "book",
      body: {
        "title": book.title,
        "ISBN": book.ISBN,
        "price": _.toNumber(book.price),
        "publicationYear": _.toNumber(book.publicationYear),
        "author": book.author,
        "genres": book.genres,
        "created": new Date()
      }
    });
  }

  async updateBook(book) {
    let fieldsToUpdate = {};
    for (let [key, value] of utils.entries(book)) {
      if (key !== 'id') {
        fieldsToUpdate[key] = value;
      }
    }
    return elasticClient.update({
      index: this.indexName,
      id: book.id,
      type: "book",
      body: {
        doc: fieldsToUpdate
      }
    });
  }

  async listBooks({page = 0, size = 10, genreId, q, sort}) {
    let indexToSearchIn = this.indexName;
    if (genreId) {
      indexToSearchIn = "genre_" + genreId;
    }
    const from = utils.calculateFrom(page, size);
    const result = await elasticClient.search({
      index: indexToSearchIn,
      type: "book",
      from: from,
      size: size,
      sort: utils.prepareSortParameters(sort),
      body: utils.prepareMatchQuery(q)
    });

    if (result.hits.hits !== undefined && result.hits.hits.length > 0) {
      return {
        total: result.hits.total,
        took: result.took,
        page: page,
        delta: size,
        list: Book.createFromElasticSearchResult(result)
      };
    } else {
      return {list: []};
    }
  }

  async getBookById(bookId) {
    const result = await elasticClient.getSource({
      index: this.indexName,
      type: "book",
      id: bookId
    }).catch(() => {
      console.error("There is no such book with id=" + bookId);
      return {};
    });
    return new Book(result);
  }

  async getGenreById(genreId) {
    const result = await elasticClient.getSource({
      index: this.indexName,
      type: "genre",
      id: genreId
    }).catch(() => {
      console.error("There is no such genre with id=" + genreId);
      return {};
    });
    return new Genre(result);
  }

  async listGenre(page = 0, size = 10, sort) {
    const from = utils.calculateFrom(page, size);
    const result = await elasticClient.search({
      index: this.indexName,
      type: "genre",
      from: from,
      size: size,
      sort: utils.prepareSortParameters(sort)
    });

    if (result.hits.hits !== undefined && result.hits.hits.length > 0) {
      return {
        total: result.hits.total,
        took: result.took,
        page: page,
        delta: size,
        list: Genre.createFromElasticSearchResult(result)
      };
    } else {
      console.warn("Can not find any genre");
      return {list: []};
    }
  }

  // ElasticSearch has also bulk insert method
  // but I wanted to learn using other API methods.
  async populateSampleData() {
    try {
      await Promise.all(genres.map(async (genre) => {
        const contents = await elasticSearchService.saveOrUpdateGenre(genre);
      }));

      await elasticClient.indices.refresh();
      let addBookChain = [];
      let index = 0;

      const result = await elasticSearchService.listGenre(0, 5);
      Array.from(result.list, (item, _) => {
        Array.from(new Array(20), (_, i) => {
          let title = bookTitles[index % 70] + " " + i % 4;
          addBookChain.push(elasticSearchService.saveOrUpdateBook({
            "title": title,
            "title.keyword": title,
            "ISBN": "ISBN-" + index,
            "price": utils.getRandomInt(10, 30) + (i + 1) % 9 / 15,
            "publicationYear": 2000 + i % 16,
            "author": {
              "firstName": firstName[utils.getRandomInt(0, 9)],
              "lastName": lastName[utils.getRandomInt(0, 9)]
            },
            "genres": {"id": item.id}
          }));
          index++;
        });
      });
      return Promise.all(addBookChain);
    } catch (err) {
      console.error(err);
      throw Error(err);
    }
  }
}

const elasticSearchService = module.exports = exports = new ElasticSearchService(require('../config/properties'));
