module.exports.firstName = [
  "Alice",
  "John",
  "Bryan",
  "Jeremy",
  "Richard",
  "James",
  "Mathew",
  "George",
  "Mary",
  "Sandra",
  "Mia"
];

module.exports.lastName = [
  "Clarkson",
  "May",
  "Hammond",
  "May",
  "Brown",
  "Collins",
  "Hamilton",
  "Torvalds",
  "Crockford",
  "Zakas",
  "Stallman"
];