module.exports = [
  {
    "name": "Horror",
    "name.keyword": "Horror",
    "description": "Horror description",
    "id": "1",
    "created": new Date()
  }, {
    "name": "Fantasy",
    "name.keyword": "Fantasy",
    "description": "Fantasy description",
    "id": "2",
    "created": new Date()

  }, {
    "name": "Drama",
    "name.keyword": "Drama",
    "description": "Drama description",
    "id": "3",
    "created": new Date()

  }, {
    "name": "Romantic",
    "name.keyword": "Romantic",
    "description": "Romantic description",
    "id": "4",
    "created": new Date()

  }, {
    "name": "Comedy",
    "name.keyword": "Comedy",
    "description": "Comedy description",
    "id": "5",
    "created": new Date()
  }
];


